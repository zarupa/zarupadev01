from flask import Flask
from datetime import datetime 


app = Flask('zarupadev01')


@app.route('/')
def hello_world():
    return 'Naber Dünya??'

@app.route('/who')
def who():
    return 'Zarupa is around!'

@app.route('/date')
def show_date():
    return 'Date is now: ' + str(datetime.now())  

if __name__ == '__main__':
    app.run()
