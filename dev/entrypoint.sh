#!/bin/bash
. /zarupadev01-venv/bin/activate

pip install -r /zarupadev01/requirements.txt
pip install gunicorn
export FLASK_APP=/zarupadev01/app.py
export FLASK_CONFIGURATION=production
#export POSTGRES_USER=zarupadev01
#export POSTGRES_PASSWORD=
#export POSTGRES_HOST=
#export POSTGRES_DB=zarupadev01
#export REDIS_HOST=
ln -s /zarupadev01 /zarupadev01-venv/lib/python3.6/site-packages/zarupadev01


# if [ $FIRST_RUN == 1 ]; then
#   cd /zarupadev01/zarupadev01
#   flask db upgrade
#   sleep 2
#   flask db migrate
#   sleep 2
#   python manage.py insert_data
#   #python fake_data.py all 10
#   exit 0

#else
#  GUNICORN_CMD_ARGS="--bind 0.0.0.0:5000 --keyfile /ssl/privkey.pem --certfile /ssl/cert.pem --workers=5 --log-level=debug --keep-alive=5" gunicorn zarupadev01.server:app
  # GUNICORN_CMD_ARGS="--forwarded-allow-ips='192.168.121.110' --bind 0.0.0.0:5000 --keyfile /ssl/privkey.pem --certfile /ssl/cert.pem --ca-certs /ssl/chain.pem --workers=5 --log-level=debug --keep-alive=5" gunicorn zarupadev01.server:app
GUNICORN_CMD_ARGS="--bind 0.0.0.0:6500 --workers=5 --log-level=debug --keep-alive=5" gunicorn zarupadev01.app:app
#  flask run --host 0.0.0.0 --port 5000
#fi

/bin/bash

#flask shell
